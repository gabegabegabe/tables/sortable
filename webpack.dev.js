const common			= require('./webpack.common.js');
const merge				= require('webpack-merge');
const Visualizer	= require('webpack-visualizer-plugin');

module.exports = merge(common, {
	mode: 'development',
	devtool: 'cheap-eval-source-map',
	module: {
		rules: [
			{
				test: /\.styl(us)?$/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: { importLoaders: 1 }
					},
					'postcss-loader',
					'stylus-loader'
				]
			}
		]
	},
	plugins: [ new Visualizer() ]
});
