const common									= require('./webpack.common.js');
const CompressionPlugin				= require('compression-webpack-plugin');
const merge										= require('webpack-merge');
const MiniCssExtractPlugin		= require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin	= require('optimize-css-assets-webpack-plugin');
const TerserPlugin					= require('terser-webpack-plugin');
const webpack									= require('webpack');

module.exports = merge(common, {
	mode: 'production',
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.styl(us)?$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: { importLoaders: 1 }
					},
					'postcss-loader',
					'stylus-loader'
				]
			}
		]
	},
	optimization: {
		minimizer: [
			new TerserPlugin({
				cache: true,
				parallel: true,
				sourceMap: true
			}),
			new OptimizeCSSAssetsPlugin()
		]
	},
	plugins: [
		new CompressionPlugin({
			test: /\.js$|\.css$|\.html$/,
			minRatio: 0.8
		}),
		new MiniCssExtractPlugin({
			filename: '[name].css'
		}),
		new webpack.optimize.AggressiveMergingPlugin(),
		new webpack.optimize.OccurrenceOrderPlugin()
	]
});
