# sortable
Sortable is a simple little library that allows you to add a 'sortable' class to
any (properly formatted) HTML table to instantly and easily make that table
sortable by clicking on its headers.

## Usage
Simply include the script on your page, and then include either `sortable` or
`sortable-with-arrows` as a class on a table, and the library will take care of
the rest!

Sortable attempts to determine the types of values in the columns in order to
sort them correctly.  Dates will be sorted by date, numbers numerically, and
strings alphabetically.

```html
	<table class="sortable">
		<thead>
			<tr>
				<th>Name</th>
				<th>Age</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Gabe</td>
				<td>31</td>
			</tr>
			<tr>
				<td>Jesus</td>
				<td>2020</td>
			</tr>
		</tbody>
	</table>
	<script src="sortable.js"></script>
```
